/* XXX */
package com.art.car;

public class Car {
    private int accelerationSpeed = 2;

    private int maxSpeed;
    private String direction;
    private int currentSpeed;
    private String brand;

    public Car() {
    }

    public Car(String brand) {
        this.brand = brand;
    }

    public Car(int maxSpeed, String brand) {
        this.maxSpeed = maxSpeed;
        this.brand = brand;
    }

    public void accelerate() {
        currentSpeed += accelerationSpeed;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public String getBrand() {
        return brand;
    }


    public void setBrand(String brand) {
        this.brand = brand;
    }
}
