package com.art.felines;

public abstract class Feline {

    private String name;
    private int numberOfPaws;
    private String color;
    private String typeOfFur;
    private String gender;


    public Feline(String name) {
        this.name = name;
    }

    public Feline(String name, int numberOfPaws, String color, String typeOfFur, String gender) {
        this.name = name;
        this.numberOfPaws = numberOfPaws;
        this.color = color;
        this.typeOfFur = typeOfFur;
        this.gender = gender;
    }


    public String getName() {
        return name;
    }

    public int getNumberOfPaws() {
        return numberOfPaws;
    }

    public String getColor() {
        return color;
    }

    public String getTypeOfFur() {
        return typeOfFur;
    }

    public String getGender() {
        return gender;
    }

    public void sleep() {
        System.out.println(name + " went to sleep...");
    }


    public abstract void destroy();
}
