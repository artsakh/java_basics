package com.art.felines;

public class Tiger extends Feline {

    private String species;
    private int numberOfStripes;


    public Tiger(String name) {
        super(name);
    }

    public Tiger(String name, int numberOfPaws, String color, String typeOfFur, String gender, String species, int numberOfStripes) {
        super(name, numberOfPaws, color, typeOfFur, gender);
        this.species = species;
        this.numberOfStripes = numberOfStripes;
    }

    public String getSpecies() {
        return species;
    }

    public int getNumberOfStripes() {
        return numberOfStripes;
    }

    @Override
    public void sleep() {
        super.sleep();
        System.out.println(getName() + " starts snorring...");
    }

    @Override
    public void destroy() {
        System.out.println(getName() + " destroys gazelle!");
    }
}
