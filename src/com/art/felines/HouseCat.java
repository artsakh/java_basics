package com.art.felines;

public class HouseCat extends Feline{

    private String typeOfCollar;


    public HouseCat(String name) {
        super(name);
    }

    public HouseCat(String name, int numberOfPaws, String color, String typeOfFur, String gender) {
        super(name, numberOfPaws, color, typeOfFur, gender);
    }


    public String getTypeOfCollar() {
        return typeOfCollar;
    }


    public void destroy() {
        System.out.println(getName() + " destroys couch!");
    }
}
