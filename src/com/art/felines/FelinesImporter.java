package com.art.felines;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FelinesImporter {


    public Feline importFeline(String importString) {
        String[] inputArray = importString.split(";");

        String felineType = inputArray[0];
        String name = inputArray[1];
        int numberOfPaws = Integer.parseInt(inputArray[2]);
        String color = inputArray[3];
        String typeOfFur = inputArray[4];
        String gender = inputArray[5];
        String species = inputArray[6];
        int numberOfStripes = Integer.parseInt(inputArray[7]);

        Feline feline = new Feline();
        if(felineType == "HOUSECAT"){
            feline = new HouseCat(name, numberOfPaws, color, typeOfFur, gender);
        } else if (felineType == "TIGER") {
            feline = new Tiger(name, numberOfPaws, color, typeOfFur, gender, species, numberOfStripes);
        }

        return feline;
    }


    public Feline[] importFelines(String fileLocation) throws FileNotFoundException {
        Scanner fileScanner = new Scanner(new File(fileLocation));

        int amountOfBoardGames = Integer.parseInt(fileScanner.nextLine());
        Feline[] felines = new Feline[amountOfBoardGames];

        int counter = 0;

        while (fileScanner.hasNext()) {
            try {
                String felineString = fileScanner.nextLine();
                Feline feline = importFeline(felineString);
                felines[counter] = feline;
                counter++;
            } catch (Exception e) {
                System.out.println("File could not be imported on line " + counter);
            }


        }


        return felines;


    }
}
