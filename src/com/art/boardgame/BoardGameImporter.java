/* XXX */
package com.art.boardgame;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class BoardGameImporter {


    public BoardGame importBoardGame(String importString) throws Exception {

        String[] inputArray = importString.split(";");

        if (inputArray.length != 4) {
            throw new Exception("Incorrect input properties");
        }

        double price = Double.parseDouble(inputArray[0]);
        String title = inputArray[1];
        int numberOfPlayers = Integer.parseInt(inputArray[2]);
        int durationInMinutes = Integer.parseInt(inputArray[3]);

        BoardGame boardGame = new BoardGame(price, title, numberOfPlayers, durationInMinutes);
        return boardGame;

    }

    public BoardGame[] importBoardGames(String fileLocation) throws FileNotFoundException {

        Scanner fileScanner = new Scanner(new File(fileLocation));
        int amountOfBoardGames = Integer.parseInt(fileScanner.nextLine());
        BoardGame[] boardGames = new BoardGame[amountOfBoardGames];

        int counter = 0;

        while (fileScanner.hasNext()) {
            try {
                String boardGameString = fileScanner.nextLine();
                BoardGame boardGame = importBoardGame(boardGameString);
                boardGames[counter] = boardGame;
                counter++;
            } catch (Exception e) {
                System.out.println("File could not be imported on line " + counter);
            }


        }


        return boardGames;

    }

}
