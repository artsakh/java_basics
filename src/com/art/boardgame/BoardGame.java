/* XXX */
package com.art.boardgame;

public class BoardGame {

    private double price;
    private String title;
    private int numberOfPlayers;
    private int durationInMinutes;

    public BoardGame(double price, String title, int numberOfPlayers, int durationInMinutes) {
        this.price = price;
        this.title = title;
        this.numberOfPlayers = numberOfPlayers;
        this.durationInMinutes = durationInMinutes;
    }

    public double getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public int getDurationInMinutes() {
        return durationInMinutes;
    }

}
