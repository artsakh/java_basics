/* XXX */
package com.art.calculator;

public class Calculator {

    public double add(double input1, double input2) {
        double result = input1 + input2;
        return result;
    }

    public double substract(double input1, double input2) {
        double result = input1 - input2;
        return result;
    }

    public double multiply(double input1, double input2) {
        double result = input1 * input2;
        return result;
    }

    public double divide(double input1, double input2) {
        double result = input1 / input2;
        return result;
    }
}
