/* XXX */
package com.art.calculator;

import java.util.Scanner;

public class CalculatorInputReceiver {

    public void ReceiveUserInput() {
        Calculator calculator = new Calculator();
        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Voer operatie 1:");

        String userInput = inputScanner.nextLine();

        String[] array = userInput.split(" ");

        String number1String = array[0];
        String operator = array[1];
        String number2String = array[2];

        double number1 = Double.parseDouble(number1String);
        double number2 = Double.parseDouble(number2String);
        double result = 0;


        if (operator.equals("+")) {
            result = calculator.add(number1, number2);
        } else if (operator.equals("-")) {
            result = calculator.substract(number1, number2);
        } else if (operator.equals("*")) {
            result = calculator.multiply(number1, number2);
        } else if (operator.equals("/")) {
            result = calculator.divide(number1, number2);
        } else {
            result = 0;
        }


        System.out.println("Het resultaat van beide cijfers is: " + result);
    }
}
