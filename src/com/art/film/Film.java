package com.art.film;

public class Film {
    private String title;
    private double budget;
    private int releaseYear;


    public Film(String title, double budget, int releaseYear) {
        this.title = title;
        this.budget = budget;
        this.releaseYear = releaseYear;
    }


    public String getTitle() {
        return title;
    }

    public double getBudget() {
        return budget;
    }

    public int getReleaseYear() {
        return releaseYear;
    }


}
