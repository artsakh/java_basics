package com.art.film;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FilmImporter {

    //private String title;
    //private double budget;
    //private int releaseYear;

    public Film importFilm(String importString) throws Exception {


        String[] inputArray = importString.split(";");

        if (inputArray.length != 3) {
            throw new Exception("Incorrect input properties");
        }

        String title = inputArray[0];
        double budget = Double.parseDouble(inputArray[1]);
        int releaseYear = Integer.parseInt(inputArray[2]);


        Film film = new Film(title, budget, releaseYear);
        return film;
    }

    public Film[] importFilms(String fileLocation) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(fileLocation));
        int amountOfFilms = Integer.parseInt(scanner.nextLine());
        Film[] films = new Film[amountOfFilms];

        int counter = 0;
        while (scanner.hasNext()) {
            try {
                String filmString = scanner.nextLine();
                Film film = importFilm(filmString);
                films[counter] = film;
                counter++;
            } catch (Exception e) {
                System.out.println("Film could not be imported on line " + counter);
            }
        }

        return films;
    }

}
