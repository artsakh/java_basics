package com.art.guess_game;

import java.util.Random;

public class Player {
    private String name;
    private int guessedNumber;

    public Player() {
    }

    public Player(String name) {
        this.name = name;
        this.guessedNumber = guessedNumber;
    }

    public String getName() {
        return name;
    }

    public int getGuessedNumber() {
        return guessedNumber;
    }

    public void guess() {
        Random rand = new Random();
        guessedNumber = rand.nextInt(10) + 1;
    }
}
