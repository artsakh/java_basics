package com.art.guess_game;

import java.util.Random;

public class GuessGame {

    public void start() {
//        Player[] players = new Player[3];

//        players[0] = new Player("Arthur");
//        players[1] = new Player("Jan");
//        players[2] = new Player("Jos");

        Player[] players = new Player[]{
                new Player("Arthur"),
                new Player("Jan"),
                new Player("Jos")
        };

        int randomNumber = new Random().nextInt(10) + 1;

//        for (int i = 0; i < players.length; i++) {
//            players[i].guess();
//        }

        boolean isGameWon = false;
        while (!isGameWon) {
            for (Player playerObject : players) {
                playerObject.guess();
                if (playerObject.getGuessedNumber() == randomNumber) {
                    System.out.println("Player " + playerObject.getName() + " has won.");
                    isGameWon = true;
                    break;
                }
            }
        }
    }
}
