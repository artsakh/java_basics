/* XXX */
package com.art;

import com.art.boardgame.BoardGame;
import com.art.boardgame.BoardGameImporter;
import com.art.felines.Feline;
import com.art.felines.HouseCat;
import com.art.felines.Tiger;
import com.art.guess_game.GuessGame;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        GuessGame game = new GuessGame();
        game.start();

//        Feline houseCat = new HouseCat("Felix");
        HouseCat houseCat = new HouseCat("Felix");
        Tiger tiger = new Tiger("Pablo");

        Feline[] felines = new Feline[2];
        felines[0] = houseCat;
        felines[1] = tiger;

        for (Feline feline : felines) {
            if (feline instanceof HouseCat) {
                HouseCat cat = (HouseCat) feline;
                System.out.println(cat.getTypeOfCollar());
                System.out.println("It's a cat");
            } else {
                feline.destroy();
            }
        }

        houseCat.destroy();
        houseCat.sleep();;

        tiger.destroy();
        tiger.sleep();


        System.out.println("----------");
    }
}
